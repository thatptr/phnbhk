
CXX = clang++

TARGET = target/out

SRCS = $(wildcard src/*.cpp impl/*.cpp include/*.cpp include/*.hpp */impl/*.hpp)

CXXFLAGS = -std=c++17 -Wall -Wpedantic

all:
	$(CXX) $(CXXFLAGS) $(SRCS)
	mv a.out target/out

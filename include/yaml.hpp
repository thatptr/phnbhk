#include <fstream>
#include <ios>
#include <string>
#include <yaml-cpp/yaml.h>

namespace file {
static void create_file(const std::string &file) {
  auto _ = std::fstream{file};
}

bool file_exists(const std::string &file);

class file {
public:
  std::string file_name;

  // Mode for use
  enum modes {
    append,
    replace,
  };

  file(const std::string &file) {
    if (!file_exists(file)) {
      create_file(file);
    }

    this->file_name = file;
  }

  void edit(const modes &mode, const std::string &data);
};

class yaml : private file {
public:
  void append(const std::string &data, const std::string &key);

  std::string get_value(const std::string &key);
};
}; // namespace file

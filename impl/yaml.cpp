#include "../include/yaml.hpp"

namespace file {
void file::edit(const modes &mode, const std::string &data) {
  if (mode == modes::append) {
    std::ofstream temp;

    temp.open(this->file_name, std::ios::app);
    temp << data;
  }

  std::ofstream temp(this->file_name);
  temp << data;
}

bool file_exists(const std::string &file) {
  std::ifstream temp(file.c_str());
  return temp.good();
}

void yaml::append(const std::string &data, const std::string &key) {
  YAML::Node node = YAML::LoadFile(this->file_name);
  node[key] = data;
}

std::string yaml::get_value(const std::string &key) {
  YAML::Node doc = YAML::LoadFile(this->file_name);
  return doc[key].as<std::string>();
}

} // namespace file
